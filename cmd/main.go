package main

import (
	"context"
	"fmt"
	"gitlab.com/pet-vadim/libs/logger"
	"gitlab.com/pet-vadim/service-dashboard/internal/repositories"
	pingrepo "gitlab.com/pet-vadim/service-dashboard/internal/repositories/ping"
	"gitlab.com/pet-vadim/service-dashboard/internal/servicies/ping"
	"gitlab.com/pet-vadim/service-dashboard/internal/transport/rest"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// @title Dashboard Service
// @version 1.0
// @description Dashboard for creating new tests. You should use JWT access token in Authorization header

// @host localhost:8080
// @BasePath /

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization

func main() {
	logger.Info("starting service dashboard app")

	db := repositories.New(getDBConfig())
	r := pingrepo.New(db)
	s := pingservice.New(r)
	h := rest.New(s, getHandlersConfig())

	srv := &http.Server{
		Addr:    ":" + h.Config.Port,
		Handler: h.InitRoutes(),
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logger.Fatal("server listen error: " + err.Error())
		}
	}()

	// Listen for the interrupt signal.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	logger.Info("received os.signal: " + (<-quit).String())

	// The context is used to inform the server it has second to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		logger.Fatal("server forced to shutdown: " + err.Error())
	}

	if err := db.Close(); err != nil {
		logger.Fatal("fail db connection close " + err.Error())
	}

	logger.Info("server exiting")
}

func getHandlersConfig() *rest.Config {
	return &rest.Config{
		Protocol: os.Getenv("SRV_PROTOCOL"),
		Host:     os.Getenv("SRV_HOST"),
		Port:     os.Getenv("SRV_PORT"),
		GinMode:  os.Getenv("SRV_GIN_MODE"),
		LogLvl:   os.Getenv("SRV_LOG_LVL"),
		JWTKey:   []byte(os.Getenv("JWT_KEY")),
		Origin: fmt.Sprintf(
			"%v://%v:%v/",
			os.Getenv("SRV_PROTOCOL"),
			os.Getenv("SRV_HOST"),
			os.Getenv("SRV_PORT")),
	}
}

func getDBConfig() *repositories.Config {
	return &repositories.Config{
		DBUsername: os.Getenv("DB_USERNAME"),
		DBPassword: os.Getenv("DB_PASSWORD"),
		DBHost:     os.Getenv("DB_HOST"),
		DBPort:     os.Getenv("DB_PORT"),
		DBTable:    os.Getenv("DB_TABLE"),
	}
}


package pingrepo

import (
	"context"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
	"gitlab.com/pet-vadim/service-dashboard/internal/servicies/ping"
)

type repository struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) pingservice.Repository {
	return &repository{db: db}
}

func (r *repository) Create(ctx context.Context, test *pingservice.Test) (*pingservice.Test, *errs.AppError) {
	query := `
	INSERT INTO ping_test
	VALUES (gen_random_uuid(), $1, $2, $3, $4, $5, $6)
	RETURNING id, name, url, wont_status_code, fail_msg, user_id, timeout_seconds;
	`
	rowx := r.db.QueryRowxContext(
		ctx,
		query,
		test.Name,
		test.URL,
		test.WontStatusCode,
		test.FailMsg,
		test.UserID,
		test.TimeoutSeconds,
	)

	var createdTest pingservice.Test
	err := rowx.StructScan(&createdTest)
	if err != nil {
		logger.Error("fail struct scan from db rowx: " + err.Error())
		return nil, errs.NewUnexpectedError("database unexpected error")
	}

	return &createdTest, nil
}

func (r *repository) Update(ctx context.Context, test *pingservice.Test) (*pingservice.Test, *errs.AppError) {
	query := `
	UPDATE ping_test
	SET name=$1, url=$2, wont_status_code=$3, fail_msg=$4, timeout_seconds=$5
	WHERE id=$5 AND user_id=$6
	`
	rowx := r.db.QueryRowxContext(
		ctx,
		query,
		test.Name,
		test.URL,
		test.WontStatusCode,
		test.FailMsg,
		test.TimeoutSeconds,
	)

	var updatedTest pingservice.Test
	err := rowx.StructScan(&updatedTest)
	if err != nil {
		logger.Error("fail struct scan from db rowx: " + err.Error())
		return nil, errs.NewUnexpectedError("database unexpected error")
	}

	return &updatedTest, nil
}

func (r *repository) Delete(ctx context.Context, id, userID string) *errs.AppError {
	query := `
	DELETE
	FROM ping_test
	WHERE id=$1 AND user_id=$2
	`
	res, err := r.db.ExecContext(ctx, query, id, userID)
	if err != nil {
		logger.Error("fail sqlx ExecContext delete ping_test" + err.Error())
		return errs.NewUnexpectedError("database unexpected error")
	}

	count, err := res.RowsAffected()
	if err != nil {
		logger.Error("fail count rows affected" + err.Error())
		return errs.NewUnexpectedError("database unexpected error")
	}

	if count != 1 {
		logger.Error("fail delete ping_test incorrect count of rows were affected" + err.Error())
		return errs.NewUnexpectedError("database unexpected error")
	}

	return nil
}

package rest_test

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	pingservice "gitlab.com/pet-vadim/service-dashboard/internal/servicies/ping"
	"gitlab.com/pet-vadim/service-dashboard/internal/transport/rest"
	mock_ping "gitlab.com/pet-vadim/service-dashboard/mocks/service/ping"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandler_Update(t *testing.T) {
	tests := []struct {
		ID      string
		name    string
		userID  string
		in      rest.UpdateInput
		out     interface{}
		outCode int
	}{
		{
			name:   "success",
			userID: "uuidUser",
			in: rest.UpdateInput{
				ID:             "uuidTestID",
				Name:           "success ya.ru",
				URL:            "https://ya.ru",
				WontStatusCode: http.StatusOK,
				FailMsg:        "something clever",
				TimeOutSeconds: 60,
			},
			out: rest.UpdateOutput{
				ID: "uuidTestID",
			},
			outCode: http.StatusOK,
		},
		{
			name:   "fail no test id",
			userID: "uuidUser",
			in: rest.UpdateInput{
				ID:             "uuidTestID",
				Name:           "success ya.ru",
				URL:            "https://ya.ru",
				WontStatusCode: http.StatusOK,
				FailMsg:        "",
				TimeOutSeconds: 60,
			},
			out: rest.ErrResponse{
				Error: "incorrect input data",
			},
			outCode: http.StatusUnprocessableEntity,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			ctr := gomock.NewController(t)
			p := mock_ping.NewMockService(ctr)
			pingTest := pingservice.Test{
				ID:             "uuidTestID",
				UserID:         tt.userID,
				Name:           tt.in.Name,
				URL:            tt.in.URL,
				WontStatusCode: tt.outCode,
				FailMsg:        tt.in.FailMsg,
				TimeoutSeconds: tt.in.TimeOutSeconds,
			}
			updatedTest := pingTest
			updatedTest.ID = "uuidTestID"
			p.EXPECT().Update(context.Background(), &updatedTest).Return(&updatedTest, nil).MinTimes(0)
			handler := rest.New(p, &rest.Config{})

			gin.SetMode(gin.TestMode)
			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)
			rest.SetUserID(c, tt.userID)
			b, err := json.Marshal(tt.in)
			if err != nil {
				t.Fatal(err.Error())
			}
			c.Request = httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(b))

			// Act
			handler.Update(c)

			// Assert
			assert.Equal(t, tt.outCode, w.Code)
			expJSON, err := json.Marshal(tt.out)
			if err != nil {
				t.Fatal(err.Error())
			}
			assert.JSONEq(t, string(expJSON), w.Body.String())

			ctr.Finish()
		})
	}
}

package rest

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
	"net/http"
)

type DeleteInput struct {
	ID string `json:"id" binding:"required"`
}

type DeleteOutput struct {
	DeletedTestID string `json:"deleted_test_id"`
}

// Delete
// @Summary Delete test
// @Security ApiKeyAuth
// @Tags Ping
// @Description Delete a ping test
// @Accept json
// @Produce json
// @Param input body DeleteInput true "id of the test"
// @Success 200 {object} DeleteOutput "success"
// @Failure 401 {object} ErrResponse "login or password is incorrect"
// @Failure 422 {object} ErrResponse "incorrect struct of request or validation failed"
// @Failure 500 {object} ErrResponse "internal server error"
// @Router /dashboard/ping/delete [post]
func (h *Handler) Delete(c *gin.Context) {
	ctx := c.Request.Context()
	var input DeleteInput

	err := c.ShouldBindJSON(&input)
	if err != nil {
		logger.Error("create ping test binding json err: " + err.Error())
		SendErrResp(c, errs.NewUnprocessableEntity("incorrect input data"))
		return
	}

	currentUserID, appErr := GetUserID(c)
	if appErr != nil {
		SendErrResp(c, appErr)
		return
	}

	appErr = h.Ping.Delete(ctx, input.ID, currentUserID)
	if appErr != nil {
		SendErrResp(c, appErr)
		return
	}

	c.JSONP(http.StatusOK, DeleteOutput{
		DeletedTestID: input.ID,
	})
}

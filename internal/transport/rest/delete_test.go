package rest_test

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pet-vadim/service-dashboard/internal/transport/rest"
	mock_ping "gitlab.com/pet-vadim/service-dashboard/mocks/service/ping"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandler_Delete(t *testing.T) {
	tests := []struct {
		name    string
		userID  string
		in      rest.DeleteInput
		out     interface{}
		outCode int
	}{
		{
			name:   "success",
			userID: "uuidUser",
			in: rest.DeleteInput{
				ID: "uuidTestID",
			},
			out: rest.DeleteOutput{
				DeletedTestID: "uuidTestID",
			},
			outCode: http.StatusOK,
		},
		{
			name:   "fail no test id",
			userID: "uuidUser",
			in: rest.DeleteInput{
				ID: "",
			},
			out: rest.ErrResponse{
				Error: "incorrect input data",
			},
			outCode: http.StatusUnprocessableEntity,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			ctr := gomock.NewController(t)
			p := mock_ping.NewMockService(ctr)
			p.EXPECT().Delete(context.Background(), tt.in.ID, tt.userID).Return(nil).MinTimes(0)
			handler := rest.New(p, &rest.Config{})

			gin.SetMode(gin.TestMode)
			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)
			rest.SetUserID(c, tt.userID)
			b, err := json.Marshal(tt.in)
			if err != nil {
				t.Fatal(err.Error())
			}
			c.Request = httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(b))

			// Act
			handler.Delete(c)

			// Assert
			assert.Equal(t, tt.outCode, w.Code)
			expJSON, err := json.Marshal(tt.out)
			if err != nil {
				t.Fatal(err.Error())
			}
			assert.JSONEq(t, string(expJSON), w.Body.String())

			ctr.Finish()
		})
	}
}

package rest

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
	ping "gitlab.com/pet-vadim/service-dashboard/internal/servicies/ping"
	"net/http"
)

type CreateInput struct {
	Name           string `json:"name" binding:"required"`
	URL            string `json:"url" binding:"required"`
	WontStatusCode int    `json:"wont_status_code" binding:"required"`
	FailMsg        string `json:"fail_msg" binding:"required"`
	TimeOutSeconds int    `json:"time_out_seconds" binding:"required"`
}

type CreateOutput struct {
	TestID string `json:"test_id"`
}

// Create
// @Summary Create test
// @Security ApiKeyAuth
// @Tags Ping
// @Description Create a ping test
// @Accept json
// @Produce json
// @Param input body CreateInput true "parameters of new test"
// @Success 200 {object} CreateOutput "success"
// @Failure 401 {object} ErrResponse "login or password is incorrect"
// @Failure 422 {object} ErrResponse "incorrect struct of request or validation failed"
// @Failure 500 {object} ErrResponse "internal server error"
// @Router /dashboard/ping/create [post]
func (h *Handler) Create(c *gin.Context) {
	ctx := c.Request.Context()
	var input CreateInput

	err := c.ShouldBindJSON(&input)
	if err != nil {
		logger.Error("create ping test binding json err: " + err.Error())
		SendErrResp(c, errs.NewUnprocessableEntity("incorrect input data"))
		return
	}

	currentUserID, appErr := GetUserID(c)
	if appErr != nil {
		SendErrResp(c, appErr)
		return
	}

	testInput := ping.Test{
		ID:             "",
		UserID:         currentUserID,
		Name:           input.Name,
		URL:            input.URL,
		WontStatusCode: input.WontStatusCode,
		FailMsg:        input.FailMsg,
		TimeoutSeconds: input.TimeOutSeconds,
	}

	test, appErr := h.Ping.Create(ctx, &testInput)
	if appErr != nil {
		SendErrResp(c, appErr)
		return
	}

	c.JSONP(http.StatusOK, CreateOutput{TestID: test.ID})
}

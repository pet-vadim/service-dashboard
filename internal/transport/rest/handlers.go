package rest

import (
	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/pet-vadim/service-dashboard/docs"
	"gitlab.com/pet-vadim/service-dashboard/internal/servicies/ping"
	"net/http"
)

type Handler struct {
	Ping   pingservice.Service
	Config *Config
}

func New(p pingservice.Service, c *Config) *Handler {
	return &Handler{
		Ping:   p,
		Config: c,
	}
}

func (h *Handler) InitRoutes() *gin.Engine {
	gin.SetMode(h.Config.GinMode)

	r := gin.New()

	r.Use(gin.Recovery())
	r.Use(JSONLogMiddleware())

	r.GET("/health", func(c *gin.Context) {
		c.String(http.StatusOK, "I am alive")
	})

	r.GET("/dashboard/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	d := r.Group("/dashboard")
	{
		pingTest := d.Group("/ping", h.CheckAuth)
		{ //nolint
			pingTest.POST("/create", h.Create)
			pingTest.POST("/update", h.Update)
			pingTest.DELETE("/delete", h.Delete)
		}
	}

	return r
}

type Config struct {
	Protocol string
	Host     string
	Port     string
	GinMode  string
	LogLvl   string
	Origin   string
	JWTKey   []byte
}

package pingservice

type Test struct {
	ID             string `json:"id" db:"id"`
	UserID         string `json:"user_id" db:"user_id"`
	Name           string `json:"name" validate:"required" db:"name"`
	URL            string `json:"url" validate:"required,url,min=5,max=2000" db:"url"`
	WontStatusCode int    `json:"wont_status_code" validate:"required" db:"wont_status_code"`
	FailMsg        string `json:"fail_msg" validate:"required,min=2,max=1000" db:"fail_msg"`
	TimeoutSeconds int    `json:"time_out_seconds" validate:"required" db:"timeout_seconds"`
}

package pingservice

import (
	"context"
	"gitlab.com/pet-vadim/libs/errs"
)

//go:generate mockgen -source=repository.go -destination=../../../mocks/service/ping/mockPingRepository.go
type Repository interface {
	Create(ctx context.Context, test *Test) (*Test, *errs.AppError)
	Update(ctx context.Context, test *Test) (*Test, *errs.AppError)
	Delete(ctx context.Context, id, userID string) *errs.AppError
}

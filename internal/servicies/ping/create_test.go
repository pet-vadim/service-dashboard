package pingservice_test

import (
	"context"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/service-dashboard/internal/servicies/ping"
	mock_ping "gitlab.com/pet-vadim/service-dashboard/mocks/service/ping"
	"net/http"
	"testing"
)

func Test_service_Create(t *testing.T) {
	tests := []struct {
		name string
		in   *pingservice.Test
		out  *pingservice.Test
		err  *errs.AppError
	}{
		{
			name: "success",
			in: &pingservice.Test{
				UserID:         "userID",
				Name:           "ping ya.ru",
				URL:            "https://ya.ru",
				WontStatusCode: http.StatusOK,
				FailMsg:        "something clever",
				TimeoutSeconds: 60,
			},
			out: &pingservice.Test{
				ID:             "testID",
				UserID:         "userID",
				Name:           "ping ya.ru",
				URL:            "https://ya.ru",
				WontStatusCode: http.StatusOK,
				FailMsg:        "something clever",
				TimeoutSeconds: 60,
			},
			err: nil,
		},
		{
			name: "fail validation url",
			in: &pingservice.Test{
				UserID:         "userID",
				Name:           "ping ya.ru",
				URL:            "invalid url",
				WontStatusCode: http.StatusOK,
				FailMsg:        "something clever",
				TimeoutSeconds: 60,
			},
			out: nil,
			err: errs.NewValidationError("Key: 'Test.URL' Error:Field validation for 'URL' failed on the 'url' tag"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			ctr := gomock.NewController(t)
			r := mock_ping.NewMockRepository(ctr)
			r.EXPECT().Create(context.Background(), tt.in).Return(tt.out, nil).MinTimes(0)
			s := pingservice.New(r)

			// Act
			test, appError := s.Create(context.Background(), tt.in)

			// Assert
			assert.Equal(t, tt.out, test)
			assert.Equal(t, tt.err, appError)

			ctr.Finish()
		})
	}
}

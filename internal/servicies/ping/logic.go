package pingservice

import (
	"context"
	"github.com/go-playground/validator/v10"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
)

type service struct {
	repo      Repository
	validator *validator.Validate
}

func New(repo Repository) Service {
	return &service{
		repo:      repo,
		validator: validator.New(),
	}
}

func (s *service) Create(ctx context.Context, test *Test) (*Test, *errs.AppError) {
	err := s.validator.Struct(test)
	if err != nil {
		logger.Info("validation err")
		return nil, errs.NewValidationError(err.Error())
	}
	return s.repo.Create(ctx, test)
}

func (s *service) Update(ctx context.Context, test *Test) (*Test, *errs.AppError) {
	err := s.validator.Struct(test)
	if err != nil {
		logger.Info("validation err")
		return nil, errs.NewValidationError(err.Error())
	}
	return s.repo.Update(ctx, test)
}

func (s *service) Delete(ctx context.Context, id, userID string) *errs.AppError {
	return s.repo.Delete(ctx, id, userID)
}

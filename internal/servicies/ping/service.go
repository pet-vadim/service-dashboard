package pingservice

import (
	"context"
	"gitlab.com/pet-vadim/libs/errs"
)

//go:generate mockgen -source=service.go -destination=../../../mocks/service/ping/mockPingService.go
type Service interface {
	Create(ctx context.Context, test *Test) (*Test, *errs.AppError)
	Update(ctx context.Context, test *Test) (*Test, *errs.AppError)
	Delete(ctx context.Context, id, userID string) *errs.AppError
}

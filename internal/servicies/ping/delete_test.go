package pingservice_test

import (
	"context"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pet-vadim/service-dashboard/internal/servicies/ping"
	mock_ping "gitlab.com/pet-vadim/service-dashboard/mocks/service/ping"
	"testing"
)

func TestService_Delete(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		// Arrange
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		r := mock_ping.NewMockRepository(ctrl)
		r.EXPECT().Delete(context.Background(), "testID", "userID").Return(nil)
		s := pingservice.New(r)

		// Act
		appError := s.Delete(context.Background(), "testID", "userID")

		// Assert
		assert.Nil(t, appError)
	})
}
